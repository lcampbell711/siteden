exports.processHyperlink = function () {
    let menuData = [
        { "name":"Home", "link": "/"},
        { "name":"Admin", "link": "admin" },
        { "name":"Test", "link": "test" },
        { "name":"NotFound", "link": "notfound" }
    ]
    return menuData;
}